<?php


class App
{
    private $input;

    /**
     * App constructor.
     * @param $input
     */
    public function __construct($input)
    {
        $this->input = $input;
    }
    public function runCommand()
    {
        if (isset($this->input[1])) {
            $input_text = $this->input[1];
            $uppercase_output = strtoupper($input_text);
            $alternative_uppercase_output = $this->makeAlternativeLetterUppercase($input_text);
            $csv_output = $this->generateCsv($input_text);
            echo  $uppercase_output."\n". $alternative_uppercase_output."\n". $csv_output;
        }else{
            echo "Give input as a single string\n";
        }

    }

    /**
     * This function will convert input string's letter to alternative uppercase letter.
     * @param $data
     * @return string
     */
    private function makeAlternativeLetterUppercase($data)
    {
        $data_array = str_split($data);
        for($i = 0; $i < count($data_array); $i++){
            if($i==0) continue;
            else{
                if($i%2 != 0){
                    $data_array[$i] = strtoupper($data_array[$i]);
                }
            }
        }
        return  implode("", $data_array);



    }

    /**
     * This function will generate csv using each character from the input.
     * @param $data
     * @return string
     */
    private function generateCsv($data)
    {
        $data_array = str_split($data);
        $csv_array = [$data_array];
        try {
            $f = fopen("output.csv", "w");
            foreach ($csv_array as $line) {
                fputcsv($f, $line);
            }
            return "Csv Successfully generated";
        }catch (Exception $exception){
            return "Something went wrong in time of generating CSV";
        }

    }

}