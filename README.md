# assignment_iprice

Assignment for Software Engineer in iprice

## Getting started

1. Clone the repository
2. Go to the project directory
3. Open command prompt or git bash.
4. Run **php.exe test.php "hello world"** this command 
5. Make sure you gave input as a string


## General Info
1. It's written in raw php
2. It can be done using many ways like using any framework like laravel.
